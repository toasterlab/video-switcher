import React, { useState, useEffect, useRef } from "react";
import "./App.css";

/* ***** EDIT SETTINGS HERE ***** */

// If true there will be a menu in the upper left allowing you to switch between video and calibration image
const showDebugMenu = false;

// Set this to the dimensions of the full frame, everything else is calculated
const frameSize = { w: 1280, h: 720 };

// Url to the 8th wall project when you click upper left quadrant
const eighthWallProjectUrl = "https://toasterlab.8thwall.app/hls-green-screen/";

// Url to the MP4 stream used for content
const videoUrl = "https://stream.vrcdn.live/live/toasterlab/index.m3u8"; //"test-video.mp4";


/* ***** DO NOT EDIT BELOW THIS LINE ***** */

const whichQuadrant = ({ x, y }) => {
  let h, v;
  if (x < frameSize.w * 0.25) {
    h = "left";
  } else {
    h = "right";
  }
  if (y < frameSize.h * 0.25) {
    v = "top";
  } else {
    v = "bottom";
  }
  if (h === "left" && v === "top") return 1;
  if (h === "right" && v === "top") return 2;
  if (h === "right" && v === "bottom") return 3;
  if (h === "left" && v === "bottom") return 4;
};

function App() {
  const videoRef = useRef(null);
  const [offset, setOffset] = useState({ left: 0, top: 0 });
  const [activeQuadrant, setActiveQuadrant] = useState(1);
  const [content, setContent] = useState("video");

  useEffect(() => {
    console.log(videoRef.current);
    setTimeout(() => videoRef.current.play(), 500);
  }, []);
  // Main Menu: Quadrants numbered clockwise from upper left
  const mainMenuActions = {
    1: () => window.open(eighthWallProjectUrl, "_blank").focus(),
    2: () => {
      setOffset({ left: -frameSize.w / 2, top: 0 });
      setActiveQuadrant(2);
    },
    3: () => {
      setActiveQuadrant(3);
      setOffset({ left: -frameSize.w / 2, top: -frameSize.h / 2 });
    },
    4: () => {
      setActiveQuadrant(4);
      setOffset({ left: 0, top: -frameSize.h / 2 });
    }
  };
  const onClick = (e) => {
    const x = e.pageX - e.currentTarget.offsetLeft;
    const y = e.pageY - e.currentTarget.offsetTop;

    switch (activeQuadrant) {
      case 1:
        mainMenuActions[whichQuadrant({ x, y }).toString()]();
        break;
      default:
        setActiveQuadrant(1);
        setOffset({ left: 0, top: 0 });
    }
  };

  return (
    <div
      className="App"
      style={{ position: "relative", backgroundColor: "#000000" }}
    >
      {showDebugMenu && (
        <div
          style={{
            position: "absolute",
            color: "white",
            padding: "5rem 0 0 5rem",
            height: "2rem",
            display: "flex"
          }}
        >
          <div onClick={() => setContent("video")}>Video</div> •
          <div onClick={() => setContent("image")}>Image</div> •
          <a href={videoUrl}>{videoUrl}</a>
        </div>
      )}
      <div
        style={{
          display: "flex",
          alignItems: "center",
          width: "100vw",
          height: "100vh"
        }}
      >
        <div
          style={{
            maxWidth: "100vw",
            border: "1px solid #222222",
            position: "relative",
            overflow: "hidden",
            width: frameSize.w / 2,
            height: frameSize.h / 2,
            margin: "auto"
          }}
          onClick={onClick}
        >
          {(content === "video" && (
            <React.Fragment>
              <video
                ref={videoRef}
                playsInline
                muted
                style={{
                  position: "absolute",
                  width: frameSize.w,
                  height: frameSize.h,

                  left: offset.left,
                  top: offset.top
                }}
              >
                <source controls src={videoUrl} type="video/mp4" />
              </video>
              {activeQuadrant === 1 && (
                <div
                  id="overlay"
                  style={{
                    position: "absolute",
                    width: frameSize.w * 0.25,
                    height: frameSize.h * 0.25,
                    background: "white",
                    margin: "auto",
                    opacity: 0.1
                  }}
                ></div>
              )}
            </React.Fragment>
          )) || (
            <img
              alt="Test Pattern"
              style={{
                position: "absolute",
                width: frameSize.w,
                height: frameSize.h,
                left: offset.left,
                top: offset.top
              }}
              src="test-pattern.png"
              draggable="false"
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
